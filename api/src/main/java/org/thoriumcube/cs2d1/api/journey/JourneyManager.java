package org.thoriumcube.cs2d1.api.journey;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.jetbrains.annotations.Unmodifiable;
import org.thoriumcube.cs2d1.api.model.TripRequestModel;

import java.util.Collection;

/**
 * Represents the manager responsible for journeys
 */
public interface JourneyManager {

    /**
     * Gets all routes within the system
     * @return the routes
     */
    @NonNull @Unmodifiable Collection<Route> getAllRoutes();

    /**
     * Gets all cities the system is aware of
     * @return the cities
     */
    @NonNull @Unmodifiable Collection<String> getAllCities();

    /**
     * Get minimum budget
     * @return the minimum budget
     */
    @NonNull Float getMinBudget();

    /**
     * Plans a journey between two cities
     * <p>Returns a collection of ordered journeys, with the first in the collection being the best.</p>
     * @param tripRequestModel the information used to request the trip
     * @return the resultant journey(s)
     */
    @NonNull @Unmodifiable Collection<Journey> planJourney(@NonNull TripRequestModel tripRequestModel);

}
