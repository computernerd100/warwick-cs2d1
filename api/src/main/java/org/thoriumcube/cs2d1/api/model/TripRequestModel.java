package org.thoriumcube.cs2d1.api.model;

import lombok.Getter;
import lombok.Setter;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class TripRequestModel {

    @Getter @Setter private @NonNull String start;
    @Getter @Setter private @NonNull String destination;
    @Getter @Setter private @NonNull String arrivalTime;
    @Getter @Setter private @Nullable Float budget;

}
