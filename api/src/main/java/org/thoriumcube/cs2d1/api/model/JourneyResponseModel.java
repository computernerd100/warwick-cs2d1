package org.thoriumcube.cs2d1.api.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.thoriumcube.cs2d1.api.journey.Journey;

import javax.money.MonetaryAmount;
import java.time.LocalTime;

@RequiredArgsConstructor
public class JourneyResponseModel {

    @Getter private final @NonNull LocalTime departureTime;
    @Getter private final @NonNull String startCity;
    @Getter private final @NonNull LocalTime arrivalTime;
    @Getter private final@NonNull String destinationCity;
    @Getter private final @NonNull String[] travelCompanies;
    @Getter private final @NonNull MonetaryAmount cost;

    public static JourneyResponseModel of(Journey journey) {
        return new JourneyResponseModel(
                journey.getDepartureTime(),
                journey.getStartCity(),
                journey.getArrivalTime(),
                journey.getDestinationCity(),
                journey.getTravelCompanies().toArray(String[]::new),
                journey.getCost()
        );
    }

}
