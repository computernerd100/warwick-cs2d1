package org.thoriumcube.cs2d1.api.journey;

import org.checkerframework.checker.nullness.qual.NonNull;

import javax.money.MonetaryAmount;
import java.time.LocalTime;
import java.util.Collection;

/**
 * Represents a journey
 * <p>A journey may consist of one or more {@link Route}s</p>
 */
public interface Journey {

    /**
     * Gets the time at which this journey departs
     * @return the departure time
     */
    @NonNull LocalTime getDepartureTime();

    /**
     * Gets the city from which this journey starts
     * @return the start city
     */
    @NonNull String getStartCity();

    /**
     * Gets the time at which this journey arrives
     * @return the arrival time
     */
    @NonNull LocalTime getArrivalTime();

    /**
     * Gets the city at which this journey arrives
     * @return the destination city
     */
    @NonNull String getDestinationCity();

    /**
     * Returns all the travel companies used during this journey
     * @return the travel companies
     */
    @NonNull Collection<String> getTravelCompanies();

    /**
     * Gets the total cost of this journey
     * @return the journey cost
     */
    @NonNull MonetaryAmount getCost();

    /**
     * Gets the {@link Route routes} which make up this journey
     * <p>A journey may consist of only a single route, in which case this method will return
     * a singleton collection of the route.</p>
     * @return the routes
     */
    @NonNull Collection<Route> getRoutes();

}
