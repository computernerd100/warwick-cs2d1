package org.thoriumcube.cs2d1.api.account;

/**
 * Represents the manager responsible for accounts
 */
public interface AccountManager {

    /**
     * Creates an account
     * @param email the email for the new account
     * @param password the password for the new account
     * @return {@code true} if account creation was successful
     */
    boolean createAccount(String email, String password);

    /**
     * Deletes an account
     * @param email the email of the account to delete
     * @return {@code true} if account deletion was successful
     */
    boolean deleteAccount(String email);

    /**
     * Attempts an account login with the specified credentials
     * @param email the email to log in with
     * @param password the password to log in with
     * @return {@code true} if the login is valid
     */
    boolean login(String email, String password);

    /**
     * Logs out an account
     * @param email the email to logout
     */
    void logout(String email);

}
