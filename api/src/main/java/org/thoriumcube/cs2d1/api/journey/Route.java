package org.thoriumcube.cs2d1.api.journey;

import org.checkerframework.checker.nullness.qual.NonNull;

import javax.money.MonetaryAmount;
import java.time.LocalTime;

/**
 * Represents a route between two cities
 */
public interface Route {

    /**
     * Gets the departure time of this route
     * @return the departure time
     */
    @NonNull LocalTime getDepartureTime();

    /**
     * Gets the city from which this route starts
     * @return the start city
     */
    @NonNull String getStartCity();

    /**
     * Gets the city at which this route ends
     * @return the end city
     */
    @NonNull String getDestinationCity();

    /**
     * Gets the arrival time of this route
     * @return the arrival time
     */
    @NonNull LocalTime getArrivalTime();

    /**
     * Gets the travel company for this route
     * @return the travel company
     */
    @NonNull String getTravelCompany();

    /**
     * Gets the cost of this route
     * @return the cost
     */
    @NonNull MonetaryAmount getCost();

}
