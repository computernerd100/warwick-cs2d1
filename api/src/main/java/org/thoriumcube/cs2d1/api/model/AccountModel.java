package org.thoriumcube.cs2d1.api.model;

import lombok.Getter;
import lombok.Setter;
import org.checkerframework.checker.nullness.qual.NonNull;

public class AccountModel {

    @Getter @Setter private @NonNull String email;
    @Getter @Setter private @NonNull String password;

}
