# CS2D1 Final Project - Instructions 

## Running the API 

Navigate to the project root directory. Issue the `./gradlew run` command which will install all the API dependencies, including the correct version of Java. The first installation will take a few minutes, subsequent runs will be faster. The terminal will appear to hang at 90% on the status bar with the status of **Executing** that indicates the API is running on `0.0.0.0:8080` but can be accessed by `localhost:8080` or `127.0.0.1:8080` as well. 

## Running the frontend 

In a separate terminal to that running the backend API, navigate from the project root directory into the `frontend` directory. `npm install yarn` will download a local copy of the yarn builder, which this project uses. `node_modules/yarn/bin/yarn` command can be used to install the dependencies and `node_modules/yarn/bin/yarn start` will start the frontend on port `3000`. The frontend will require access to the REST API (`localhost:8080`) locally, otherwise an `Internal server error` will be displayed.

 

## Running the tests 

`./gradlew test` will run all of the unit and integration tests. It should report a `BUILD SUCCESSFUL in xs` if the tests are successful. 

 

## Generating test schedules 

Navigate to the `scripts` directory of the root project. `python3 schedule_generator.py` will generate a random schedule in the current working directory. This schedule can be used to test the system by replacing the `<project root>/core/src/main/resources/cs2d1-schedule.csv` with the `cs2d1-schedule.csv` file created in the current working directory and then starting the API as detailed above.

 
