package org.thoriumcube.cs2d1.core;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thoriumcube.cs2d1.core.account.AccountManagerImpl;
import org.thoriumcube.cs2d1.core.gson.MoneyTypeAdapterFactory;
import org.thoriumcube.cs2d1.core.journey.JourneyManagerImpl;
import org.thoriumcube.cs2d1.core.journey.RouteImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static spark.Spark.awaitInitialization;
import static spark.Spark.awaitStop;

/*
 * Unit tests for the Server class.
 * 
 * These tests mainly consist of testing a specific endpoints exists and their
 * error conditions, where applicable, are functioning correctly.
 */
@ExtendWith(MockitoExtension.class)
public class ServerTest {
    Server server;

    @Mock
    JourneyManagerImpl mockJourneyManager;
    @Mock
    AccountManagerImpl mockAccountManager;

    private final Gson gson = Converters
            .registerLocalTime(new GsonBuilder().registerTypeAdapterFactory(new MoneyTypeAdapterFactory())).create();

    /*
     * Starts the server before each test and waits for the server
     * to be ready to accept requests before allowing the test to
     * run.
     */
    @BeforeEach
    public void setUp() {
        server = new Server(mockJourneyManager, mockAccountManager);
        server.start();
        awaitInitialization();
    }

    /*
     * Closes the server after each test has run and waits for
     * the server to fully close to avoid a race condition
     * where the port is still in use at the setUp of the
     * next test and the test fails stochastically.
     */
    @AfterEach
    public void tearDown() {
        server.shutdown();
        awaitStop();
    }

    /*
     * Tests that the server returns the list of cities
     * held by the JourneyManager(Imp) when a GET request
     * is sent to the "all-cities" endpoint.
     */
    @Test
    public void testAllCitiesEndpoint() throws Exception {

        // Returns specified cities list from the JourneyManager
        // to isolate this unit test from the JourneyManager/CSV
        // implementation
        when(mockJourneyManager.getAllCities()).thenReturn(List.of("City1", "City2"));

        APIResponseData r = apiEndpointRequest("all-cities");

        // check the response is 200
        assertEquals(200, r.response);

        // check correct JSON list is returned
        assertEquals("[\"City1\",\"City2\"]", r.data.toString());
    }

    /*
     * Tests that the server returns the list of all
     * routes (timetable entries) when a GET request
     * is sent to the "all-routes" endpoint.
     */
    @Test
    public void testAllRoutesEndpoint() throws Exception {
        RouteImpl route1 = new RouteImpl(LocalTime.of(21, 30), "CityA", "CityB",
                LocalTime.of(22, 30), "TravelA",
                Money.of(1.5, "GBP"));

        RouteImpl route2 = new RouteImpl(LocalTime.of(22, 33), "CityB", "CityC",
                LocalTime.of(22, 55), "TravelA",
                Money.of(0.90, "GBP"));

        // returns the above routes array from the JourneyManager
        // to isolate this unit test from the JourneyManager/CSV
        // implementation
        when(mockJourneyManager.getAllRoutes()).thenReturn(List.of(route1, route2));

        APIResponseData r = apiEndpointRequest("all-routes");

        // check the response is 200
        assertEquals(200, r.response);

        RouteImpl[] responseRoutes = gson.fromJson(r.data.toString(), RouteImpl[].class);

        // check correct JSON list of routes is returned
        assertEquals(route1, responseRoutes[0]);
        assertEquals(route2, responseRoutes[1]);
    }

    /*
     * Tests that the server returns the result
     * of the JourneyManager.getMinBudget() method
     * when a GET request is sent to the "all-routes" endpoint.
     */
    @Test
    public void testMinBudgetEndpoint() throws Exception {
        when(mockJourneyManager.getMinBudget()).thenReturn(7.25f);
        APIResponseData response = apiEndpointRequest("min-budget");

        // check the response is 200
        assertEquals(200, response.response);

        // check correct value is returned
        assertEquals("7.25", response.data.toString());
    }

    /*
     * Tests that the server responds correctly
     * to an invalid request to the "plan" endpoint.
     * Specifically, if a non-JSON request is made.
     */
    @Test
    public void testPlanEndpointNoJsonError() throws Exception {
        HttpURLConnection r = makeConnection("plan", "text/plain", "POST");

        // check the response is 400 (because we sent an invalid request)
        assertEquals(400, r.getResponseCode());
    }

    /*
     * Test the API can deal with an empty
     * JSON request to the "plan" endpoint.
     * Specifically, an empty list should be
     * returned.
     */
    @Test
    public void testPlanEndpointEmptyRequest() throws Exception {
        APIResponseData r = apiEndpointRequest("plan", "application/json", "POST");

        // check that a response code 200 is returned because the API
        // should be able to deal with a blank request.
        assertEquals(200, r.response);

        // check that an empty request returns no planned routes
        assertEquals("[]", r.data.toString());
    }

    public static HttpURLConnection makeConnection(String endpoint, String contentType, String method)
            throws Exception {
        URL url = new URL("http://localhost:8080/api/" + endpoint);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(method);
        con.setRequestProperty("Content-Type", contentType);

        return con;
    }

    public static APIResponseData apiEndpointRequest(String endpoint) throws Exception {
        return apiEndpointRequest(endpoint, "text/plain", "GET");
    }

    public static APIResponseData apiEndpointRequest(String endpoint, String contentType, String method)
            throws Exception {
        HttpURLConnection con = makeConnection(endpoint, contentType, method);

        int status = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();

        return new APIResponseData(content, status);

    }

    @AllArgsConstructor
    static class APIResponseData {
        StringBuffer data;
        int response;
    }

}


