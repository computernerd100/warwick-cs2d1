package org.thoriumcube.cs2d1.core.journey;

import com.google.common.collect.ImmutableSet;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.thoriumcube.cs2d1.api.journey.Journey;
import org.thoriumcube.cs2d1.api.journey.Route;
import org.thoriumcube.cs2d1.api.model.TripRequestModel;

import java.time.LocalTime;
import java.util.Collection;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/*
 * Integration tests for the JourneyManagerImpl class.
 * 
 * These tests require elements of the GSON utilities to load an example timetable so these tests are integration. 
 */
public class JourneyManagerImplTest {

    // JourneyManagerImpl will load from the resources
    // folder and so will load the mock timetable situated
    // within
    private JourneyManagerImpl journeyManager = new JourneyManagerImpl();

    // JourneyManagerImpl will load from the resources
    // folder and so will load the minimal unit timetable
    // that attempts to unit test the JourneyManagerImpl class.
    // NOTE: these are not actual unit tests because they still
    // rely on the CSV utilities to load the data
    private JourneyManagerImpl journeyManagerUnit = new JourneyManagerImpl("/unit-tests.csv");

    /*
     * Test to assert that all the cities defined with the CSV are returned.
     */
    @Test
    public void testGetAllCities() {
        String[] citiesUnit = { "Apple", "Banana", "Orange" };

        assertEquals(ImmutableSet.copyOf(citiesUnit), journeyManagerUnit.getAllCities());
    }

    /*
     * Test to assert that the correct minimum budget is returned.
     * This will be the smallest cost across all jorneys defined in
     * the timetable CSV file.
     */
    @Test
    public void testGetMinBudget() {
        assertEquals(2.5f, journeyManagerUnit.getMinBudget());
    }

    /*
     * Test to assert that the two routes defined within the CSV are returned
     */
    @Test
    public void testGetAllRoutes() {
        RouteImpl route1 = new RouteImpl(LocalTime.of(6, 0), "Apple", "Banana",
                LocalTime.of(6, 30), "A",
                Money.of(5, "GBP"));

        RouteImpl route2 = new RouteImpl(LocalTime.of(6, 31), "Banana", "Orange",
                LocalTime.of(6, 39), "B",
                Money.of(2.5, "GBP"));

        Collection<Route> result = journeyManagerUnit.getAllRoutes();

        assertTrue(result.contains(route1));
        assertTrue(result.contains(route2));

        // checks that we only have the two we expect
        assertEquals(2, result.size());
    }

    private static Stream<Arguments> provideTestPlanJourneyParams() {
        return Stream.of(
                Arguments.of("06:56", 1),
                Arguments.of("06:55", 1),
                Arguments.of("07:00", 2),
                Arguments.of("07:01", 2),
                Arguments.of("06:30", 0));
    }

    /*
     * Test to check that the routing algorithm correctly identifies
     * journeys that are feasible and within the users constraints.
     */
    @ParameterizedTest
    @MethodSource("provideTestPlanJourneyParams")
    public void testPlanJourney(String time, int numExpectedJourneys) {
        TripRequestModel tripRequest = new TripRequestModel();
        tripRequest.setStart("Apple");
        tripRequest.setDestination("Pear");
        tripRequest.setArrivalTime(time);

        Collection<Journey> journeys = journeyManager.planJourney(tripRequest);

        assertEquals(numExpectedJourneys, journeys.size());
    }

}
