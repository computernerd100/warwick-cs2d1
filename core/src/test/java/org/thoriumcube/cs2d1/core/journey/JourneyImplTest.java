package org.thoriumcube.cs2d1.core.journey;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.thoriumcube.cs2d1.api.journey.Route;

import javax.money.MonetaryAmount;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JourneyImplTest {
    private JourneyImpl journey;

    @BeforeEach
    public void setUp() {
        Route[] testRoutes = {
                new RouteImpl(LocalTime.of(21, 30), "CityA", "CityB",
                        LocalTime.of(22, 30), "TravelA",
                        Money.of(1.5, "GBP")),

                new RouteImpl(LocalTime.of(22, 33), "CityB", "CityC",
                        LocalTime.of(22, 55), "TravelB",
                        Money.of(0.90, "GBP"))
        };

        journey = new JourneyImpl(LocalTime.of(21, 20), "CityA", LocalTime.of(22, 40), "CityC",
                Arrays.asList(testRoutes));
    }

    @Test
    public void testGetTravelCompanies() {
        Collection<String> travelCompanies = journey.getTravelCompanies();

        assertTrue(travelCompanies.contains("TravelA"));
        assertTrue(travelCompanies.contains("TravelB"));
        assertEquals(2, travelCompanies.size());
    }

    @Test
    public void testGetCost() {
        MonetaryAmount cost = journey.getCost();

        assertEquals(Money.of(2.40, "GBP"), cost);
    }

}
