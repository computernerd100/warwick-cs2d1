package org.thoriumcube.cs2d1.core.csv;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.javamoney.moneta.Money;
import org.thoriumcube.cs2d1.api.journey.Route;
import org.thoriumcube.cs2d1.core.journey.RouteImpl;

import javax.money.MonetaryAmount;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class CSV {

    private static final String[] HEADERS = { "departure_time", "start_city", "destination", "arrival_time",
            "travel_company", "cost" };

    /**
     * Load a CSV file timetable and return in a set.
     * 
     * @param csvName: the name of the CSV file, located in the resources,
     *                 directory.
     */
    public static Set<Route> getRoutesFromCSV(String csvName) throws IOException {
        Reader in = new InputStreamReader(Objects.requireNonNull(CSV.class.getResourceAsStream(csvName)));

        CSVFormat format = CSVFormat.DEFAULT.builder().setHeader(HEADERS).setSkipHeaderRecord(true).build();

        return format.parse(in).stream().map(CSV::csvToRoute).collect(Collectors.toSet());
    }

    private static Route csvToRoute(CSVRecord record) {
        LocalTime departureTime = LocalTime.parse(record.get("departure_time").trim());
        String startCity = record.get("start_city").trim();
        String destinationCity = record.get("destination").trim();
        LocalTime arrivalTime = LocalTime.parse(record.get("arrival_time").trim());
        String travelCompany = record.get("travel_company").trim();
        MonetaryAmount cost = Money.of(Float.parseFloat(record.get("cost").trim()), "GBP");
        return new RouteImpl(departureTime, startCity, destinationCity, arrivalTime, travelCompany, cost);
    }
}
