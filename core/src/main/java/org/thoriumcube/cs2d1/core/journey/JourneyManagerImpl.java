package org.thoriumcube.cs2d1.core.journey;

import com.google.common.collect.ImmutableSet;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.javamoney.moneta.Money;
import org.jetbrains.annotations.Unmodifiable;
import org.thoriumcube.cs2d1.api.journey.Journey;
import org.thoriumcube.cs2d1.api.journey.JourneyManager;
import org.thoriumcube.cs2d1.api.journey.Route;
import org.thoriumcube.cs2d1.api.model.TripRequestModel;
import org.thoriumcube.cs2d1.core.csv.CSV;
import org.thoriumcube.cs2d1.core.util.ParetoSet;

import javax.money.MonetaryAmount;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JourneyManagerImpl implements JourneyManager {

    private final Set<Route> routes;
    private final Set<String> cities;
    private final Map<String, List<Route>> routesMap;

    public JourneyManagerImpl() {
        this("/cs2d1-schedule.csv");
    }

    public JourneyManagerImpl(String csvName) {
        this.routes = loadRoutes(csvName);
        this.cities = this.routes.stream().flatMap(r -> Stream.of(r.getStartCity(), r.getDestinationCity())).collect(Collectors.toSet());
        this.routesMap = this.cities.stream().collect(Collectors.toMap(Function.identity(), c -> this.routes.stream().filter(r -> r.getStartCity().equals(c)).collect(Collectors.toList())));
    }

    private static Set<Route> loadRoutes(String csvName) {
        try {
            return CSV.getRoutesFromCSV(csvName);
        } catch (Exception e) {
            e.printStackTrace();
            return new HashSet<>();
        }
    }

    @Override
    public @NonNull @Unmodifiable Collection<Route> getAllRoutes() {
        return ImmutableSet.copyOf(this.routes);
    }

    @Override
    public @NonNull @Unmodifiable Collection<String> getAllCities() {
        return ImmutableSet.copyOf(this.cities);
    }

    @Override
    public @NonNull Float getMinBudget() {
        return this.routes.stream().map(Route::getCost).min(MonetaryAmount::compareTo).orElseThrow().getNumber().floatValue();
    }

    @Override
    public @NonNull @Unmodifiable Collection<Journey> planJourney(@NonNull TripRequestModel tripRequestModel) {
        MonetaryAmount budget = tripRequestModel.getBudget() != null ? Money.of(tripRequestModel.getBudget(), "GBP") : Money.of(Integer.MAX_VALUE, "GBP");
        LocalTime targetArrivalTime = LocalTime.parse(tripRequestModel.getArrivalTime());

        List<Journey> journeys = this.routesMap.get(tripRequestModel.getStart()).stream()
                .map(Route::getDepartureTime)
                .distinct()
                .map(startTime -> getCompleteJourneys(tripRequestModel.getStart(), tripRequestModel.getDestination(), targetArrivalTime, budget, startTime))
                .flatMap(ParetoSet::stream)
                .distinct()
                .collect(Collectors.toList());

        return JourneyRank.orderJourneys(journeys, tripRequestModel);
    }

    private ParetoSet<JourneyImpl> getCompleteJourneys(String startCity, String destinationCity, LocalTime targetArrivalTime, MonetaryAmount budget, LocalTime startTime) {
        Stack<JourneyBuilder> journeyBuilderStack = new Stack<>();
        ParetoSet<JourneyImpl> completeJourneys = new ParetoSet<>();
        HashMap<String, ParetoSet<RouteImpl>> initialConnections = new HashMap<>();
        this.routesMap.get(startCity).stream()
                .filter(r -> !r.getDepartureTime().isBefore(startTime))
                .forEach(r -> {
                    if (!initialConnections.containsKey(destinationCity)) initialConnections.put(destinationCity, new ParetoSet<>());
                    initialConnections.get(destinationCity).add((RouteImpl) r);
                });
        initialConnections.values().forEach(set -> set.forEach(route -> {
            JourneyBuilder builder = new JourneyBuilder(targetArrivalTime, Integer.MAX_VALUE, budget);
            if (builder.addRoute(route)) journeyBuilderStack.push(builder);
        }));

        while (!journeyBuilderStack.isEmpty()) {
            JourneyBuilder builder = journeyBuilderStack.pop();
            if (builder.getCurrentStation().equals(destinationCity)) {
                completeJourneys.add((JourneyImpl) builder.toJourney());
                continue;
            }
            Set<String> previousStations = builder.getPreviousStations();
            HashMap<String, ParetoSet<RouteImpl>> potentialFutures = new HashMap<>();
            this.routesMap.get(builder.getCurrentStation()).stream()
                    .filter(r -> (!previousStations.contains(r.getDestinationCity())) && (!r.getDepartureTime().isBefore(builder.getArrivalTime())))
                    .forEach(r -> {
                        if (!potentialFutures.containsKey(r.getDestinationCity())) potentialFutures.put(r.getDestinationCity(), new ParetoSet<>());
                        potentialFutures.get(r.getDestinationCity()).add((RouteImpl) r);
                    });
            potentialFutures.values().forEach(set -> set.forEach(route -> {
                JourneyBuilder futuresBuilder = new JourneyBuilder(builder);
                if (futuresBuilder.addRoute(route)) journeyBuilderStack.push(futuresBuilder);
            }));
        }
        return completeJourneys;
    }

}
