package org.thoriumcube.cs2d1.core;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import org.thoriumcube.cs2d1.api.account.AccountManager;
import org.thoriumcube.cs2d1.api.journey.JourneyManager;
import org.thoriumcube.cs2d1.api.model.AccountModel;
import org.thoriumcube.cs2d1.api.model.JourneyResponseModel;
import org.thoriumcube.cs2d1.api.model.TripRequestModel;
import org.thoriumcube.cs2d1.core.gson.MoneyTypeAdapterFactory;

import java.util.stream.Collectors;

import static spark.Spark.*;

public class Server {

    private final JourneyManager journeyManager;
    private final AccountManager accountManager;
    private final Gson gson = Converters.registerLocalTime(new GsonBuilder().registerTypeAdapterFactory(new MoneyTypeAdapterFactory())).create();

    @Inject
    public Server(JourneyManager journeyManager, AccountManager accountManager) {
        this.journeyManager = journeyManager;
        this.accountManager = accountManager;
    }

    void start() {
        port(8080); // Set port to 8080 and start server
        registerEndpoints();
    }

    void shutdown() {
        stop();
    }

    private void registerEndpoints() {
        before((req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "*");
        });
        options("/*", (req, res) -> {
            String accessControlRequestHeaders = req.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) res.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            String accessControlRequestMethod = req.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) res.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            return "OK";
        });
        get("/hello", (req, res) -> "Hello World"); // Basic route for testing
        get("/coffee", (req, res) -> {
            res.status(418);
            return "";
        });
        path("/api", () -> {
           get("/all-cities", (req, res) -> {
               res.type("application/json");
               return gson.toJson(journeyManager.getAllCities());
           });
           get("/all-routes", (req, res) -> {
               res.type("application/json");
               return gson.toJson(journeyManager.getAllRoutes());
           });
           get("/min-budget", (req, res) -> gson.toJson(journeyManager.getMinBudget()));
           post("/plan", (req, res) -> {
               if (!req.contentType().equals("application/json")) {
                   halt(400, "Content type invalid");
               }
               return gson.toJson(journeyManager.planJourney(gson.fromJson(req.body(), TripRequestModel.class))
                       .stream().map(JourneyResponseModel::of).collect(Collectors.toList()));
           });
           exception(NullPointerException.class, (e, req, res) -> halt(400, e.getMessage()));
           post("/login", (req, res) -> {
               if (!req.contentType().equals("application/json")) {
                   halt(400, "Content type invalid");
               }
               AccountModel accountCredentials = gson.fromJson(req.body(), AccountModel.class);
               boolean success = accountManager.login(accountCredentials.getEmail(), accountCredentials.getPassword());
               if (success) {
                   res.status(200);
               } else {
                   halt(401, "Invalid login credentials");
               }
               return "";
           });
           post("/register", (req, res) -> {
               if (!req.contentType().equals("application/json")) {
                   halt(400, "Content type invalid");
               }
               AccountModel accountCredentials = gson.fromJson(req.body(), AccountModel.class);
               boolean success = accountManager.createAccount(accountCredentials.getEmail(), accountCredentials.getPassword());
               if (success) {
                   res.status(201);
               } else {
                   halt(409, "Account already exists");
               }
               return "";
           });
        });
    }

}
