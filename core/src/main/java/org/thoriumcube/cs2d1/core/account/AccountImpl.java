package org.thoriumcube.cs2d1.core.account;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.thoriumcube.cs2d1.core.util.SecureUtils;

@RequiredArgsConstructor
public class AccountImpl {

    @Getter
    private final @NonNull String email;
    private final byte[] salt;
    private final @NonNull String pwHash;

    public boolean validateLogin(String password) {
        return this.pwHash.equals(SecureUtils.hashPassword(password, this.salt));
    }

}
