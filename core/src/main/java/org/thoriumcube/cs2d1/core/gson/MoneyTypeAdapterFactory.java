package org.thoriumcube.cs2d1.core.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import lombok.RequiredArgsConstructor;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.MonetaryAmountFactory;

@RequiredArgsConstructor
public class MoneyTypeAdapterFactory implements TypeAdapterFactory {

    private final MonetaryAmountFactory<? extends MonetaryAmount> monetaryAmountFactory;

    public MoneyTypeAdapterFactory() {
        this(Monetary.getDefaultAmountFactory());
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        final Class<T> clazz = (Class<T>) type.getRawType();

        if (MonetaryAmount.class.isAssignableFrom(clazz)) {
            return (TypeAdapter<T>) new MonetaryAmountAdapter(monetaryAmountFactory);
        } else if (CurrencyUnit.class.isAssignableFrom(clazz)) {
            return (TypeAdapter<T>) new CurrencyUnitAdapter();
        }

        return null;
    }
}
