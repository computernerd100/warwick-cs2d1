package org.thoriumcube.cs2d1.core.gson;

import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.UnknownCurrencyException;
import java.io.IOException;

class CurrencyUnitAdapter extends TypeAdapter<CurrencyUnit> {

    @Override
    public void write(JsonWriter out, CurrencyUnit value) throws IOException {

        if (value == null) {
            out.nullValue();
            return;
        }

        out.value(value.getCurrencyCode());
    }

    @Override
    public CurrencyUnit read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        }

        try {
            return Monetary.getCurrency(in.nextString());
        } catch (UnknownCurrencyException e) {
            throw new JsonSyntaxException(e);
        }
    }
}
