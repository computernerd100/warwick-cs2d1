package org.thoriumcube.cs2d1.core.journey;

import org.thoriumcube.cs2d1.api.journey.Journey;
import org.thoriumcube.cs2d1.api.model.TripRequestModel;

import javax.money.MonetaryAmount;
import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.reverseOrder;
import static java.util.Map.Entry;

public final class JourneyRank {

    private static final float EPSILON = 0.001f;

    private final float lowestCost;
    private final float highestCost;
    private final float averageCost;
    private final Duration fastestTime;
    private final Duration slowestTime;
    private final Duration averageTime;
    private final LocalTime targetArrivalTime;

    private JourneyRank(Collection<Journey> journeys, LocalTime targetArrivalTime) {
        this.lowestCost = journeys.stream().map(Journey::getCost).min(MonetaryAmount::compareTo).orElseThrow().getNumber().floatValue();
        this.highestCost = journeys.stream().map(Journey::getCost).max(MonetaryAmount::compareTo).orElseThrow().getNumber().floatValue();
        this.averageCost = (lowestCost + highestCost) / 2;
        this.fastestTime = journeys.stream().map(JourneyRank::getDuration).min(Duration::compareTo).orElseThrow();
        this.slowestTime = journeys.stream().map(JourneyRank::getDuration).max(Duration::compareTo).orElseThrow();
        this.averageTime = slowestTime.plus(fastestTime).dividedBy(2);
        this.targetArrivalTime = targetArrivalTime;
    }

    public static Collection<Journey> orderJourneys(Collection<Journey> journeys, TripRequestModel tripRequestModel) {
        if (journeys.isEmpty()) return journeys;
        JourneyRank journeyRank = new JourneyRank(journeys, LocalTime.parse(tripRequestModel.getArrivalTime()));
        return journeys.stream()
                .collect(Collectors.groupingBy(journeyRank::rankJourney))
                .entrySet().stream()
                .sorted(reverseOrder(Entry.comparingByKey()))
                .map(Entry::getValue)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private int rankJourney(Journey journey) {
        int result = 0;
        result += rankCost(journey.getCost().getNumber().floatValue());
        result += rankDuration(getDuration(journey));
        result += rankTargetArrivalTime(journey.getArrivalTime());
        return result;
    }

    private int rankCost(float cost) {
        if (Math.abs(averageCost - cost) < EPSILON) return 0;
        int adjuster = 0;
        if (Math.abs(cost - lowestCost) < EPSILON) adjuster += 3;
        if (Math.abs(cost - highestCost) < EPSILON) adjuster -= 3;
        return (Math.round((averageCost - cost) / 5)) + adjuster;
    }

    private int rankDuration(Duration journeyTime) {
        if (journeyTime.compareTo(averageTime) == 0) return 0;
        long difference = averageTime.get(ChronoUnit.SECONDS) - journeyTime.get(ChronoUnit.SECONDS);
        int adjuster = 0;
        if (journeyTime.equals(fastestTime)) adjuster += 5;
        if (journeyTime.equals(slowestTime)) adjuster -= 5;
        return (int) Math.floorDiv(difference, 15 * 60) + adjuster;
    }

    private int rankTargetArrivalTime(LocalTime journeyArrivalTime) {
        if (journeyArrivalTime.compareTo(targetArrivalTime) == 0) return 5;
        long difference = journeyArrivalTime.until(targetArrivalTime, ChronoUnit.MINUTES);
        return (int) Math.floorDiv(difference, 15) * -1;
    }

    private static Duration getDuration(Journey journey) {
        return Duration.ofMinutes(journey.getDepartureTime().until(journey.getArrivalTime(), ChronoUnit.MINUTES));
    }

}
