package org.thoriumcube.cs2d1.core.journey;

import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.javamoney.moneta.Money;
import org.thoriumcube.cs2d1.api.journey.Journey;
import org.thoriumcube.cs2d1.api.journey.Route;
import org.thoriumcube.cs2d1.core.util.ParetoItem;

import javax.money.MonetaryAmount;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
public class JourneyImpl implements Journey, ParetoItem {

    private final @NonNull LocalTime departureTime;
    private final @NonNull String startCity;
    private final @NonNull LocalTime arrivalTime;
    private final @NonNull String destinationCity;
    private final @NonNull Collection<Route> routes;

    public static JourneyImpl of(List<Route> routes) {
        return new JourneyImpl(
                routes.get(0).getDepartureTime(),
                routes.get(0).getStartCity(),
                routes.get(routes.size() - 1).getArrivalTime(),
                routes.get(routes.size() - 1).getDestinationCity(),
                routes
        );
    }

    public static JourneyImpl of(Route route) {
        return new JourneyImpl(
                route.getDepartureTime(),
                route.getStartCity(),
                route.getArrivalTime(),
                route.getDestinationCity(),
                Collections.singleton(route)
        );
    }

    public JourneyImpl(@NonNull LocalTime departureTime, @NonNull String startCity, @NonNull LocalTime arrivalTime, @NonNull String destinationCity, @NonNull Collection<Route> routes) {
        this.departureTime = departureTime;
        this.startCity = startCity;
        this.arrivalTime = arrivalTime;
        this.destinationCity = destinationCity;
        this.routes = routes;
    }

    @Override
    public @NonNull Collection<String> getTravelCompanies() {
        return getRoutes().stream().map(Route::getTravelCompany).collect(Collectors.toSet());
    }

    @Override
    public @NonNull MonetaryAmount getCost() {
        return getRoutes().stream().map(Route::getCost).reduce(Money.of(0, "GBP"), MonetaryAmount::add);
    }

    @Override
    public int getSteps() {
        return this.routes.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JourneyImpl journey = (JourneyImpl) o;
        return departureTime.equals(journey.departureTime)
                && startCity.equals(journey.startCity) &&
                arrivalTime.equals(journey.arrivalTime) &&
                destinationCity.equals(journey.destinationCity) &&
                routes.equals(journey.routes) &&
                getTravelCompanies().equals(journey.getTravelCompanies()) &&
                getCost().equals(journey.getCost());
    }

    @Override
    public int hashCode() {
        return Objects.hash(departureTime, startCity, arrivalTime, destinationCity, routes, getTravelCompanies(), getCost());
    }
}
