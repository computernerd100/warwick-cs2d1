package org.thoriumcube.cs2d1.core.journey;

import org.javamoney.moneta.Money;
import org.thoriumcube.cs2d1.api.journey.Journey;
import org.thoriumcube.cs2d1.api.journey.Route;
import org.thoriumcube.cs2d1.core.util.ParetoItem;

import javax.money.MonetaryAmount;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class JourneyBuilder implements ParetoItem {

    private final List<Route> routes;

    private final LocalTime targetArrivalTime;
    private final int targetSteps;
    private final MonetaryAmount targetBudget;

    public JourneyBuilder(LocalTime targetArrivalTime, int targetSteps, MonetaryAmount targetBudget) {
        this.targetArrivalTime = targetArrivalTime;
        this.targetSteps = targetSteps;
        this.targetBudget = targetBudget;
        this.routes = new ArrayList<>();
    }

    public JourneyBuilder(JourneyBuilder builder) {
        this.targetArrivalTime = builder.targetArrivalTime;
        this.targetSteps = builder.targetSteps;
        this.targetBudget = builder.targetBudget;
        this.routes = new ArrayList<>(builder.routes);
    }

    @Override
    public MonetaryAmount getCost() {
        return this.routes.stream().map(Route::getCost).reduce(Money.of(0, "GBP"), MonetaryAmount::add);
    }

    @Override
    public int getSteps() {
        return routes.size();
    }

    @Override
    public LocalTime getArrivalTime() {
        return this.routes.get(this.routes.size() - 1).getArrivalTime();
    }

    public Set<String> getPreviousStations() {
        return this.routes.stream().map(Route::getStartCity).collect(Collectors.toSet());
    }

    public String getCurrentStation() {
        return this.routes.get(this.routes.size() - 1).getDestinationCity();
    }

    public boolean addRoute(Route route) {
        if (this.getCost().add(route.getCost()).isGreaterThan(targetBudget) || route.getArrivalTime().compareTo(targetArrivalTime) > 0) return false;
        this.routes.add(route);
        return true;
    }

    public Journey toJourney() {
        return JourneyImpl.of(this.routes);
    }
}
