package org.thoriumcube.cs2d1.core.journey;

import lombok.Data;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.thoriumcube.cs2d1.api.journey.Route;
import org.thoriumcube.cs2d1.core.util.ParetoItem;

import javax.money.MonetaryAmount;
import java.time.LocalTime;

@Data
public class RouteImpl implements Route, ParetoItem {

    private final @NonNull LocalTime departureTime;
    private final @NonNull String startCity;
    private final @NonNull String destinationCity;
    private final @NonNull LocalTime arrivalTime;
    private final @NonNull String travelCompany;
    private final @NonNull MonetaryAmount cost;

    @Override
    public int getSteps() {
        return 1;
    }
}
