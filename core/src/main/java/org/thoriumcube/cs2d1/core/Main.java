package org.thoriumcube.cs2d1.core;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.thoriumcube.cs2d1.core.di.GuiceModule;

public class Main {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new GuiceModule());
        Server server = injector.getInstance(Server.class);
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread(server::shutdown));
    }

}
