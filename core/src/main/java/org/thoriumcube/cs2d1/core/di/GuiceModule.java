package org.thoriumcube.cs2d1.core.di;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import org.thoriumcube.cs2d1.api.account.AccountManager;
import org.thoriumcube.cs2d1.api.journey.JourneyManager;
import org.thoriumcube.cs2d1.core.Server;
import org.thoriumcube.cs2d1.core.account.AccountManagerImpl;
import org.thoriumcube.cs2d1.core.journey.JourneyManagerImpl;

public class GuiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(JourneyManager.class).to(JourneyManagerImpl.class).in(Scopes.SINGLETON);
        bind(AccountManager.class).to(AccountManagerImpl.class).in(Scopes.SINGLETON);
        bind(Server.class).in(Scopes.SINGLETON);
    }
}
