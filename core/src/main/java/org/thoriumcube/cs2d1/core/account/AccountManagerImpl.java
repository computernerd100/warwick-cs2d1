package org.thoriumcube.cs2d1.core.account;

import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.Gson;
import org.thoriumcube.cs2d1.api.account.AccountManager;
import org.thoriumcube.cs2d1.core.util.SecureUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AccountManagerImpl implements AccountManager {

    private final Map<String, AccountImpl> storedAccounts = new HashMap<>();
    private final Set<String> loggedInAccounts = new HashSet<>();
    private final File accountStore;
    private final ExecutorService saverService = Executors.newFixedThreadPool(1, new ThreadFactoryBuilder().setNameFormat("Saver Service %d").setDaemon(true).build());

    public AccountManagerImpl() {
        this.accountStore = new File("accounts.json");
        try {
            initialiseStore(accountStore);
            readStore(accountStore);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Runtime.getRuntime().addShutdownHook(new Thread(saverService::shutdown));
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void initialiseStore(File accountStore) throws IOException {
        if (!accountStore.exists()) {
            accountStore.createNewFile();
        }
    }

    private void readStore(File accountStore) throws IOException {
        Gson gson = new Gson();
        Reader reader = Files.newBufferedReader(Paths.get(accountStore.getPath()));
        List<AccountImpl> accounts = gson.fromJson(reader, new TypeToken<List<AccountImpl>>() {}.getType());
        if (accounts == null || accounts.isEmpty()) return;
        accounts.forEach(account -> storedAccounts.put(account.getEmail(), account));
    }

    private void saveStore(File accountStore) throws IOException {
        Gson gson = new Gson();
        String jsonString = gson.toJson(storedAccounts.values());
        try (Writer writer = Files.newBufferedWriter(Paths.get(accountStore.getPath()))) {
            writer.write(jsonString);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private AccountImpl newAccount(String email, String password) {
        byte[] salt = SecureUtils.generateSalt();
        String hashedPassword = SecureUtils.hashPassword(password, salt);
        return new AccountImpl(email, salt, hashedPassword);
    }

    public boolean createAccount(String email, String password) {
        if (storedAccounts.containsKey(email)) return false;
        storedAccounts.put(email, newAccount(email, password));
        saverService.execute(() -> {
            try {
                saveStore(this.accountStore);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return true;
    }

    public boolean deleteAccount(String email) {
        if (storedAccounts.containsKey(email)) {
            storedAccounts.remove(email);
            return true;
        } else {
            return false;
        }
    }

    public boolean login(String email, String password) {
        AccountImpl account = storedAccounts.get(email);
        if (account == null) return false; // Account doesn't exist
        if (this.loggedInAccounts.contains(email)) return false; // Account already logged in
        if (account.validateLogin(password)) {
            this.loggedInAccounts.add(email);
            return true;
        } else {
            return false; // Password incorrect
        }
    }

    public void logout(String email) {
        this.loggedInAccounts.remove(email);
    }

    public void logoutAll() {
        this.loggedInAccounts.clear();
    }

}
