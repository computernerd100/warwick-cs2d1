package org.thoriumcube.cs2d1.core.util;

import javax.money.MonetaryAmount;
import java.time.LocalTime;

public interface ParetoItem {

    MonetaryAmount getCost();

    LocalTime getArrivalTime();

    int getSteps();

}
