package org.thoriumcube.cs2d1.core.util;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class ParetoSet<T extends ParetoItem> implements Iterable<T> {

    private final List<T> itemList = new ArrayList<>();

    public void add(T toAdd) {
        List<T> toRemove = new ArrayList<>();
        boolean loopBroken = false;
        for (T item : itemList) {
            boolean a1 = toAdd.getArrivalTime().compareTo(item.getArrivalTime()) <= 0;
            boolean a2 = toAdd.getArrivalTime().compareTo(item.getArrivalTime()) == 0;

            boolean b1 = toAdd.getCost().isLessThanOrEqualTo(item.getCost());
            boolean b2 = toAdd.getCost().isEqualTo(item.getCost());

            boolean c1 = toAdd.getSteps() <= item.getSteps();
            boolean c2 = toAdd.getSteps() == item.getSteps();

            boolean atLeastOneBetter = (a1 && !a2) || (b1 && !b2) || (c1 && !c2);
            if (!atLeastOneBetter) {
                loopBroken = true;
                break;
            }

            if (a1 && b1 && c1) toRemove.add(item);
        }
        if (!loopBroken) {
            itemList.add(toAdd);
        }

        itemList.removeAll(toRemove);
    }

    @Override
    public @NonNull Iterator<T> iterator() {
        return itemList.iterator();
    }

    public Stream<T> stream() {
        return itemList.stream();
    }

}
