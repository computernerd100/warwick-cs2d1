"""This script generates an example timetable in the users current working directory"""

import csv
from datetime import timedelta
import random


MIN_CITIES = 5
"""Minimum number of cities that a timetable will contain"""
MAX_CITIES = 10
"""Maximum number of cities that a timetable will contain"""

MIN_ROUTES = 100
"""Minimum number of routes (timetable entries) the resulting file will contain"""
MAX_ROUTES = 200
"""Maximum number of routes (timetable entries) the resulting file will contain"""

MIN_COST = 2
"""Minimum cost a route (trip from A to B) can cost"""
MAX_COST = 20
"""Maximum cost a route (trip from A to B) can cost"""

# timedelta may not be the canonical way to represent time in this context
# but it provides better mathematical operational support
EARLIEST_TIME = timedelta(hours=6)
"""The earliest possible time that will be a start time"""
LATEST_TIME = timedelta(hours=22, minutes=30)
"""The latest possible time that will be an end time"""
TIME_STEP = timedelta(minutes=10)
"""Determines the resolution of times (e.g. how many times are in the possible set of start/end times)"""

CITY_NAMES = [
    "Apple",
    "Banana",
    "Cucumber",
    "Dates",
    "Strawberry",
    "Fennel",
    "Ginger",
    "Eggsy",
    "Satsuma",
    "Pepper",
]

TRAVEL_COMPANIES = ["TravelA", "TravelB", "TravelC"]

CSV_HEADINGS = [
    "departure time",
    "start city",
    "destination",
    "arrival time",
    "travel company",
    "cost",
]


def timedelta_to_string(time: timedelta) -> str:
    """Converts a time into a string that can be parsed by the Java application.
    
    It assumes that timedelta's provided SHOULD be of the format HH:MM:SS.
    This functions behaviour is undefined for a timedelta that specifies days/microseconds."""

    time_str = str(time)
    return time_str.rjust(8, "0")


if __name__ == "__main__":
    num_cities = random.randint(MIN_CITIES, MAX_CITIES)
    cities = CITY_NAMES[:num_cities]

    num_routes = random.randint(MIN_ROUTES, MAX_ROUTES)

    times = []  # {c: c == EARLIEST_TIME + k*TIME_STEP ^ k in N ^ c <= LATEST_TIME}
    current_time = EARLIEST_TIME
    while current_time + TIME_STEP <= LATEST_TIME:
        times.append(current_time)
        current_time += TIME_STEP

    mu_start_time_idx = len(times) / 2
    std_start_time_idx = len(times) / 6

    routes = []
    for _ in range(num_routes):
        start_city = random.choice(cities)
        destination_city = random.choice(
            [city for city in cities if city != start_city]
        )  # we don't want a route between a city and itself

        start_time_idx = int(random.gauss(mu_start_time_idx, std_start_time_idx))

        # make sure an illegal start index is not chosen
        if start_time_idx < 0:
            start_time_idx = 0
        elif start_time_idx >= len(times) - 4:  # -4 because it shouldn't start too late
            start_time_idx = len(times) - 5

        end_times = times[start_time_idx + 1 :]
        mu_end_time_idx = len(end_times) / 4

        end_time_idx = int(random.expovariate(1 / mu_end_time_idx))

        # make sure an illegal end time index is not chosen
        if end_time_idx < 0:
            end_time_idx = 0
        elif end_time_idx >= len(end_times):
            end_time_idx = len(end_times) - 1

        start_time = timedelta_to_string(times[start_time_idx])
        arrival_time = timedelta_to_string(end_times[end_time_idx])

        travel_company = random.choice(TRAVEL_COMPANIES)

        # pick a random number and scale it into the cost range
        cost = round((MAX_COST - MIN_COST) * random.random() + MIN_COST, 1)

        routes.append(
            [
                start_time,
                start_city,
                destination_city,
                arrival_time,
                travel_company,
                cost,
            ]
        )
    routes.insert(0, CSV_HEADINGS)  # add the headers to the top of the file
    with open("cs2d1-schedule.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerows(routes)

