plugins {
    `java-library`
    id("com.github.johnrengelman.shadow") version "7.1.2" apply false
}

buildscript {
    repositories {
        maven("https://plugins.gradle.org/m2/")
    }
    dependencies {
        classpath("io.freefair.gradle:lombok-plugin:6.4.3")
        classpath("gradle.plugin.com.github.johnrengelman:shadow:7.1.2")
    }
}

allprojects {

    group = "org.thoriumcube.cs2d1"
    
    apply(plugin = "java")
    apply(plugin = "io.freefair.lombok")
    apply(plugin = "com.github.johnrengelman.shadow")

    repositories {
        mavenCentral()
    }

    dependencies {
        implementation("org.checkerframework:checker-qual:3.22.0")
        implementation("org.jetbrains:annotations:23.0.0")
        implementation("org.javamoney:moneta:1.4.2")
        implementation("org.apache.commons:commons-csv:1.9.0")
    }

    tasks.withType<JavaCompile> {
        options.encoding = Charsets.UTF_8.name()
    }

    tasks.withType<Javadoc> {
        options.encoding = Charsets.UTF_8.name()
    }

    tasks.withType<ProcessResources> {
        filteringCharset = Charsets.UTF_8.name()
    }

    java {
        toolchain {
            toolchain.languageVersion.set(JavaLanguageVersion.of(11))
        }
    }

}