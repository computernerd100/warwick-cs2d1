import React from 'react'
import Container from 'react-bootstrap/esm/Container'

function AccountRoot() {
  return (
    <Container className='flex mt-5'>
      <div className='account-box'>
        <h3>
          You are logged in :)
        </h3>
        <p>
          This Is Coming SoonTM
        </p>
      </div>
    </Container>
  )
}

export default AccountRoot