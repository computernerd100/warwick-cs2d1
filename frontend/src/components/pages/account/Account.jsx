import React, { useEffect } from 'react'
import { useAtom } from 'jotai'
import { emailAtom } from '../../../jotai'
import Container from 'react-bootstrap/esm/Container';
import AccountRoot from './AccountRoot';
import AccountCreation from './AccountCreation';
function Account() {
  const [email,] = useAtom(emailAtom);

  const render = ()=>{
    if(email){
      return (
        <AccountRoot/>
      )
    }
   return (
    <AccountCreation/>
   )
  }
  return (
    <Container>
      {render()}
    </Container>
  )
}

export default Account