import React, { useState } from 'react'
import Container from 'react-bootstrap/esm/Container'
import InputGroup from 'react-bootstrap/esm/InputGroup'
import FormControl from 'react-bootstrap/esm/FormControl'
import Button from 'react-bootstrap/esm/Button';
import toast from 'react-hot-toast'
import {login,regex,register} from '../../../api'
import Form from 'react-bootstrap/Form'
function AccountCreation() {
  const [loginPanel, ] = useState(window.location.href.split("/")?.[3]==='login');
  const [email,setEmail] = useState('');
  const [password,setPassword] = useState('');
  const [password2, setPassword2] = useState('')
  const handleClick = ()=>{
    if(loginPanel){
      login(email,password).then(res=>toast(res.message)).catch(err=>toast.error(err.message,'login'))
    }else{
      register(email,password).then(res=>toast(res.message)).catch(err=>toast.error(err.message,'register'))
    }
  }
  const loginText = loginPanel?'register':'login';
  const emailInvalid = email && !regex.email.test(email)
  const passwordInvalid = password && !regex.password.test(password)
  return (
    <Container className='flex mt-5'>
      <div className='account-box'>
        <h3>
          {loginPanel ? 'Login..' : 'Create Account..'}
        </h3>
        <div className='flex mt-4 column' style={{ gap: "2em" }}>
          <InputGroup className="user-login">
            <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
            <FormControl
              placeholder="Email"
              aria-label="Email"
              aria-describedby="basic-addon1"
              onChange={(e)=>setEmail(e.target.value)}
              isInvalid={emailInvalid}
            />
            {emailInvalid && (<Form.Control.Feedback type="invalid">
    Please enter a valid e-mail address.
  </Form.Control.Feedback>)}
          </InputGroup>
          <InputGroup className="user-login">
            <InputGroup.Text id="basic-addon1">#</InputGroup.Text>
            <FormControl
              type='password'
              placeholder="Password"
              aria-label="password"
              aria-describedby="basic-addon1"
              onChange={(e)=>setPassword(e.target.value)}
              isInvalid={passwordInvalid}
            />
            {passwordInvalid && (<Form.Control.Feedback type="invalid">
    Password doesn't meet requirements. 
  </Form.Control.Feedback>)}
          </InputGroup>
          {(!loginPanel && password) && (
            <InputGroup className="user-login">
            <InputGroup.Text id="basic-addon1">#</InputGroup.Text>
            <FormControl
              type='password'
              placeholder="Confirm Password"
              aria-label="password"
              aria-describedby="basic-addon1"
              onChange={(e)=>setPassword2(e.target.value)}
              isInvalid={password!==password2}
            />
            {password!==password2 && (<Form.Control.Feedback type="invalid">
    Passwords don't match
  </Form.Control.Feedback>)}
          </InputGroup>
          )}
        </div>
        <div className='flex mt-5 column' style={{ gap: "1em" }}>
        <Button onClick={handleClick} disabled={!loginPanel && password!==password2}>
            {loginPanel ? 'Login' : 'Register'}
          </Button>
          <div className='flex gap start'>
          
          <a className='soonTM'>
            Forgot Password
          </a>
          <a href={`/${loginText}`}> Or {loginText}...</a>
          </div>
        
        </div>
      </div>

    </Container>
  )
}

export default AccountCreation