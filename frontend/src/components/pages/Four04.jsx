import React from 'react'
import Button from "react-bootstrap/Button"
function Four04() {
  return (
    <div style={{background:"radial-gradient(circle at 50%,white 25%,red 50%,#ff6961)",display:"flex",flexDirection:"column", justifyContent:"center",alignItems:"center",width:"100%",height:"auto",minHeight:"100vh",position:"relative",margin:"auto",}}>
      <h1 style={{fontSize:"7em",color:'#ff6961',filter:"drop-shadow(0px 2px 4px black)"}}>
        404
      </h1>
      <h5 style={{color:'#ff6961',filter:"drop-shadow(0px 2px 1px black)"}}>
        Page Not Found
      </h5>
      <Button href={"/"} variant="outline-success" size="sm">Go Back To Safety</Button>
    </div>
  )
}

export default Four04