import React from 'react'
import Container from 'react-bootstrap/esm/Container'
import Divider from '../layout/Divider'
import ShowCase from '../support/ShowCase'
function Home() {
  return (
    <div>
      <div className="bg-light" style={{display:"flex", marginBottom: "2rem",paddingTop:"2.5rem",paddingBottom:"1rem", boxShadow: "0 0.1rem 0.2em lightgrey" }}>
      <Container>
        <h2 >Welcome to the world of travel! </h2>
        <p style={{paddingLeft:"1em",fontSize:"1.2em"}}>Memories that last a lifetime</p>
      </Container>
      <a class="btn btn-primary shadow-sm" href="/trip-planner" role="button" style={{marginRight:"2rem",height:"2.5em"}}>Book Now!</a>
      </div>
     
      <div className=''>
      <Divider><h5>
            Imagine If..
          </h5>
          </Divider>
        <ShowCase />
        <Container className="mt-5 position-relative d-flex flex-column justify-content-center align-middle">
          <Divider><h5>
            Lets get started..
          </h5>
          </Divider>
        </Container>
      </div>
      <div className=" mt-4" style={{width:"100%",height:"100%"}}>
      <Container >
            <h4 className="center" >
              Travel Now
            </h4>
            <p className="center" style={{paddingLeft:"1em",fontSize:"1.2em"}}>
              Explore the world of travel and get inspired to travel with us!
            </p>
      </Container>
      <Container style={{marginTop:"3em",display:"flex",flexDirection:"column"}}>
        <h4 className='mb-4'>
          Discover and save
        </h4>
        <a href="/trip-planner">
        <img
            className="d-block w-100 rounded-3 shadow scontent"
            src="https://picsum.photos/1000/300"
            alt="deals"
          />
          <div className="rounded"style={{position:'relative',bottom:"4em",left:"4em",backdropFilter:"blur(1em)",width:"15em",display:"flex",justifyContent:"start",alignItems:"start",padding:"0.3em"}}>
            <h5 className="text-light">Deals of a lifetime..</h5>
          </div>
        </a>
       
      </Container>
      
      </div>
    
    </div>
  )
}

export default Home