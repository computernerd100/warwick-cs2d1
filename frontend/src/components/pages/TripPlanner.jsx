import { useAtom } from 'jotai'
import React, { useState, useMemo, useEffect } from 'react'
import { Typeahead } from 'react-bootstrap-typeahead'
import Container from 'react-bootstrap/esm/Container'
import Divider from '../layout/Divider'
import { destinationsAtom, timesAtom, journeyAtom, loadedAtom, minBudgetAtom } from '../../jotai'
import Button from 'react-bootstrap/esm/Button'
import Journey from '../support/Journey'
import Spinner from 'react-bootstrap/Spinner'
import { loadData, plan, regex } from '../../api'
import toast from 'react-hot-toast'
import Card from 'react-bootstrap/Card'
import Placeholder from 'react-bootstrap/Placeholder'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
function TripPlanner() {
  const [destinations, setDestinations] = useAtom(destinationsAtom);
  const [times, setTimes] = useAtom(timesAtom);
  const [journey,setJourney] = useAtom(journeyAtom);
  const [loaded, setLoaded] = useAtom(loadedAtom)
  const [origin, setOrigin] = useState('');
  const [destination, setDestination] = useState('');
  const [destinationTime, setDestinationTime] = useState('');
  const [apiCalled, setApiCalled] = useState(false);
  const [budget, setBudget] = useState('');
  const [minBudget, setMinBudget] = useAtom(minBudgetAtom)
  useEffect(() => {
    if (!loaded) {
      loadData(setDestinations, setTimes, setMinBudget).then(res => {
        setLoaded(true)
      }).catch(err => {console.log('error:C',err);toast.error(err.msg??"Internal Server Error",{id:'load-error'})})
    }
  }, [destinations, times])
  const handleSearch = async () => {
    if (!destination || !origin || !destinationTime) {
      const message = new String('Please select a ');
      message.concat(origin ? '' : 'origin, ')
      message.concat(destination ? '' : 'destination, ')
      message.concat(destinationTime ? '' : 'time')
      message.splice(message.length - 2, 2)
      toast.error(message,'trip-search-error')
      return;
    }
    console.log("Searching for trip",origin,destination,destinationTime,budget)
    //TODO: journey or all
    setApiCalled(true)
    await plan(origin, destination, destinationTime, budget).then(res => {
      //TODO: DO IT
      console.log("RESPONSE",res)
      setJourney(res.data)
      toast.success(res.data.length===0?'No Journeys Found':'Trips Retrieved')
      setApiCalled(false)
    }).catch(err => {
      console.log("ERROR",err)
      toast.error(err.message, 'load-error')});
   
  }
  const renderGhost = () => {
    return Array.from({ length: 4 }).map(x => (
      <Col>
        <Card>
          <Card.Body>
            <Placeholder as={Card.Title} animation="glow">
              <Placeholder xs={6} />
            </Placeholder>
            <Placeholder as={Card.Text} animation="glow">
              <Placeholder xs={7} /> <Placeholder xs={4} /> <Placeholder xs={4} />{' '}
              <Placeholder xs={6} /> <Placeholder xs={8} />
            </Placeholder>
            <Placeholder.Button variant="primary" xs={6} />
          </Card.Body>
        </Card>
      </Col>
    ))
  }
  const renderResult = useMemo(() => {
    if (journey.length === 0 && destinations.length > 0) {
      if (apiCalled) {return renderGhost()};
      return <Journey title='No Journey Found' cost={0.00} description="We couldn't find a journey that matched your criteria, please try again." disabled/>
    }
    const result = [];
    for (let i = 0; i < journey.length; i++) {
      const title = `${journey[i].startCity} to ${journey[i].destinationCity}`
      const departureTime = journey[i].departureTime.slice(0,-3);
      const arrivalTime = journey[i].arrivalTime.slice(0,-3);
 
      const cost = journey?.[i]?.cost?.amount??0.00
      
      result.push(<Col><Journey travelCompanies={journey[i].travelCompanies}recommended={i==0} title={title} start={`Departs: ${departureTime}`} destination={`Arrives: ${arrivalTime}`} cost={cost} key={'journey' + i} /></Col>)
    }
    console.log("JOURNEY",journey)
    return result;
  }, [journey,apiCalled])
  
  return (
    <div>
      <div className="bg-light" style={{ display: "flex", marginBottom: "2rem", paddingTop: "2.5rem", paddingBottom: "1rem", boxShadow: "0 0.1rem 0.2em lightgrey" }}>
        <Container>
          <div style={{ backgroundColor: 'whitesmoke', padding: "1em", borderRadius: "16px", width: "30rem", borderColor: "", boxShadow: "inset 0 0 1rem darkgrey, 0 0 0.5em black" }}>
            <h2 >One Stop Travel Shop </h2>
            <p style={{ paddingLeft: "1em", fontSize: "1.2em" }}>Lets Travel Now! ✈️</p>
          </div>
        </Container>
      </div>
      <Divider>
        <h5>
          Start Exploring
        </h5>
      </Divider>
      <Container className="background mt-4" style={{ width: '100em', minWidth: 'fit-content' }}>

        <div className='flex column start travel-container ' style={{ padding: "1.5rem", gap: "0.25rem", backgroundColor: "darkslategray", borderRadius: "2rem" }}>
          <h3 style={{
            color: 'whitesmoke',
            marginLeft: "2.5rem",
            marginBottom: "1em",
            fontWeight: "bold"
          }}>
            Where Are You Headed
          </h3>
          <div className="flex gap" style={{ marginLeft: "4rem" }}>
            <div id='inputTravel' className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">From</span>
              </div>
              <Typeahead
              id='origin'
                placeholder='Apple'
                isLoading={destinations.length === 0}
                disabled={destinations.length===0}
                value={origin}
                onChange={(selected) =>{
                  
                  setOrigin(selected?.[0]??'')}}
                options={destinations.filter(dest => dest.toLowerCase().includes(destination.toLowerCase()))}
              />
            </div>
            <div id='inputTravel' className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">To</span>
              </div>
              <Typeahead
              id='destination'
                placeholder='Cucumber'
                isLoading={destinations.length === 0}
                disabled={destinations.length===0}
                value={destination}
                onChange={(selected) => setDestination(selected?.[0]??'')}
                options={destinations.filter(dest => !dest.toLowerCase()!==origin.toLowerCase())}
              /></div>
          </div>
          <div className="flex gap" style={{ marginLeft: "4rem" }}>
            {/* <div id='inputTravel' className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">Time</span>
              </div>
              <Typeahead
                placeholder='11:00'
                isLoading={times.length == 0}
                disabled={times.length==0}
                onChange={(selected) =>
                  setOriginTime(selected)}
                options={times.filter(time => time.toLowerCase().includes(destinationTime.toLowerCase()))}
              />
            </div>  */}

            <div id='inputTravel' className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">Time</span>
              </div>
              <Typeahead
              id='time'
                placeholder='12:00'
                isLoading={times.length === 0}
                disabled={times.length===0}
                value={destinationTime}
                onChange={(selected) => {
                  const time = selected?.[0]??''
                  if(time && regex.time.test(time)){return}
                  setDestinationTime(time)
                }}
                options={times}
              />
            </div>
            <div className='flex start' style={{with:"100%"}}>
          <InputGroup id='inputTravel' className="mb-3" >
            <InputGroup.Text>Budget</InputGroup.Text>
            <InputGroup.Text>£</InputGroup.Text>
            <FormControl placeholder={minBudget} value={budget} onChange={(e)=>{
              //
              console.log(regex.budget.test(e.target.value))
              if(regex.budget.test(e.target.value) || e.target.value === ''){
                setBudget(e.target.value)
              }
            }} aria-label="Amount (to the nearest pound)" disabled={times.length==0}/>
          </InputGroup>
          </div>
          </div>
         
         
          <div className="flex end" >
            <Button onClick={handleSearch} className="btn btn-primary" style={{ marginLeft: "2rem", marginTop: "1rem" }} disabled={!origin || !destination || !destinationTime}>
              {destinations.length == 0 ? (<div><Spinner as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true" /> loading</div>) : "Search"}
            </Button>

          </div>

        </div>
      </Container>
      <Container className='mt-5'>
        <Row xs={1} md={4} className="g-4">
          {renderResult}
          
        </Row>
      </Container>
    </div>
  )
}

export default TripPlanner