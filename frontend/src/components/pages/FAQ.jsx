import React from 'react'
import Container from 'react-bootstrap/esm/Container'

function FAQ() {
  // i know i could have made this a css list.. idc its a dead FAQ page.. dont @ me
  return (
    <div className="bg-dark" >
      <div className={"bg-light p-2"}>
        <Container>
          <h1>
            Frequently Asked Questions:
          </h1>
        </Container>
      </div>
      <div className={"bg-body p-2 shadow mt-1 mb-1"} >
        <Container style={{ marginTop: "0.2rem" }}>
          <h4 style={{ paddingLeft: "1rem" }}>
            Lorem:
          </h4>
          <p style={{ paddingLeft: "2rem" }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </Container>
      </div>
      <div className={"bg-light p-2 shadow mt-1 mb-1"} >
        <Container>
          <h4 style={{ paddingLeft: "1rem" }}>
            Ipsum:
          </h4>
          <p style={{ paddingLeft: "2rem" }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </Container>
      </div>
      <div className={"bg-body p-2 shadow mt-1 mb-1"} >
        <Container >
          <h4 style={{ paddingLeft: "1rem" }}>
            Dolor:
          </h4>
          <p style={{ paddingLeft: "2rem" }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </Container>
      </div>
      <div className={"bg-light p-2 shadow mt-1 mb-1"} >
        <Container >
          <h4 style={{ paddingLeft: "1rem" }}>
            Sit:
          </h4>
          <p style={{ paddingLeft: "2rem" }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </Container>
      </div>
      <div className={"bg-body p-2 shadow mt-1 mb-1"} >
        <Container >
          <h4 style={{ paddingLeft: "1rem" }}>
            Amet:
          </h4>
          <p style={{ paddingLeft: "2rem" }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </Container>
      </div>
      <div className={"bg-light p-2 shadow mt-1 mb-1"} >
        <Container >
          <h4 style={{ paddingLeft: "1rem" }}>
          Consectetur:
          </h4>
          <p style={{ paddingLeft: "2rem" }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </Container>
      </div>
    </div>
  )
}

export default FAQ