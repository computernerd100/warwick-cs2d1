import React from 'react'

function Divider(props) {
  return (
    <div className="divider">{props?.children}</div>
  )
}

export default Divider