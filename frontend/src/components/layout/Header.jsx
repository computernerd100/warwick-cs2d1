import React from 'react'
import  Navbar  from 'react-bootstrap/Navbar'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Container  from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
function Header() {
  return (
<Navbar className={"shadow"}collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Container>
  <Navbar.Brand href="/" style={{display:"flex",justifyContent:"center",gap:'0.2em'}}>
    <p style={{alignItems:'center',justifyContent:"center",textAlign:'center',margin:'0 auto'}}></p>
    <img style={{transform:'scale(3)'}}alt=""
          src="/traveller.png"
          width="30"
          height="30"
          className="d-inline-block align-top">
  </img>
  </Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">
      <Nav.Link href="/trip-planner">Trip Planner</Nav.Link>
      <Nav.Link href="/faq">FAQ</Nav.Link>
      <NavDropdown title="Support" id="collasible-nav-dropdown">
        <NavDropdown.Item href="/support/pricing">Pricing</NavDropdown.Item>
        <NavDropdown.Item href="/support/account">account</NavDropdown.Item>
        <NavDropdown.Item href="/support/Travel">Travel</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="/support/contact-us">Contact Us</NavDropdown.Item>
      </NavDropdown>
    </Nav>
    <Nav className="">
      <NavDropdown title="Login" id="collasible-nav-dropdown">
        <NavDropdown.Item href="/login">Login</NavDropdown.Item>
        <NavDropdown.Item href="/register">Register</NavDropdown.Item>
      </NavDropdown>
    </Nav>
  </Navbar.Collapse>
  </Container>
</Navbar>
  )
}

export default Header