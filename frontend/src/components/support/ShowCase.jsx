import React from 'react'
import Carousel from 'react-bootstrap/Carousel'
import Container from 'react-bootstrap/esm/Container'

function ShowCase() {
  return (
    <Container className={"p-5"} style={{ marginTop: '1rem' }}>
      <Carousel fade={true} controls={true} className={"shadow p-2 border-5 border-dark rounded-pill "} >
        <Carousel.Item className={"border border-2 border-dark rounded-pill"} style={{minWidth:"800px",minHeight:"400px"}}>
          <img
            className="d-block w-100 rounded-pill"
            src="https://placeimg.com/1920/1080/nature"
            alt="First slide"
          />
          <Carousel.Caption className="border border-2 border-light rounded-pill" style={{ backdropFilter: 'blur(4em)' }}>

            <h3 className="fw-bold">Do Stuff ✨</h3>
            <p className="fw-normal">Go do stuff, and leave your house for a change!</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item className={"border border-2 border-dark rounded-pill "} style={{minWidth:"800px",minHeight:"400px"}}>
          <img
            className="d-block w-100 rounded-pill"
            src="https://placeimg.com/1920/1080/arch"
            alt="Second slide"
          />

          <Carousel.Caption className="border  border-2 border-light rounded-pill" style={{ backdropFilter: 'blur(4em)' }}>
            <h3 className="fw-bold">See Things 👀</h3>
            <p className="fw-normal">Why not visit interesting places and see interesting things?</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item className={"border border-2 border-dark rounded-pill"} style={{minWidth:"800px",minHeight:"400px"}}>
          <img
            className="d-block w-100 rounded-pill"
            src="https://placeimg.com/1920/1080/animals"
            alt="Third slide"
          />

          <Carousel.Caption className="border  border-2 border-light rounded-pill " style={{ backdropFilter: 'blur(4em)' }}>
            <h3 className="fw-bold">Animals 🐻</h3>
            <p className="fw-normal">Go and see some animals, they are cute (probably)!</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </Container>
  )
}

export default ShowCase