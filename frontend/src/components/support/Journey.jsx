import React,{useState} from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import Badge from 'react-bootstrap/Badge'
import Button from 'react-bootstrap/Button'
import toast from 'react-hot-toast'
import { Spinner } from 'react-bootstrap'
import { useAtom } from 'jotai'
import { emailAtom } from '../../jotai'
function Journey(props) {
  const recommended = props?.recommended;
  const {disabled}=props;
  const title = props?.title??'loading..';
  const start = props?.start??'loading..';
  const destination = props?.destination??'loading..';
  const travelCompanies = props?.travelCompanies??'loading..';
  const cost = props?.cost??'0.00';
  const [email,]=useAtom(emailAtom)
  const [emailSent,setEmailSent]=useState(false)
  const handelGo= ()=>{
    toast.success('Coming SoonTM')
  }

  const emailQuote = ()=>{
    if(email && !emailSent){
      setEmailSent(true)
      toast.success("Email Support Coming Soon 😊",'email-soonTM')
    }else{
      toast.error('Please Login')
    }
  }
  return (
        <Card>
        
          <Card.Img variant="top" src={`https://placeimg.com/1920/1080/arch/grayscale`} />
          {(recommended) && <Badge className='overlay-card-badge' bg="success" text="light">
          Recommended
        </Badge>}
          <Card.Body>
            <Card.Title className='cardTitle'>{title} <span className={`cardCost ${recommended>0?'money':'broke'}`}>£{cost}</span></Card.Title>
            <Card.Text>
              <div className="cardTitle column flexgap-1 start">
              <div className="flex flexgap-1">
              <span className='cardDescription'>{start}</span>
              <span className='cardDescription'>{destination}</span>
              </div>
              <div className='flex column start gap-1'>
                Travel Companies:
                <div className='flex start flexgap-1' style={{marginLeft:'0.5em'}}>
                  {travelCompanies.map(x=> <span key={x} className='card-travelCompany'>{x}</span>)}
                </div>
              </div>
              
              </div>
              
            </Card.Text>
            <Button variant="primary" onClick={handelGo} disabled={disabled}>{disabled?<Spinner animation="grow" variant="info" size="sm"/>:'Book'}</Button>
            {email && <Button variant='secondary' onClick={emailQuote} disabled={disabled}>{disabled?<Spinner animation="grow" variant="info" size="sm"/>:'Email Trip'}</Button>}
          </Card.Body>
        </Card>
  )
}

export default Journey