import {atom} from 'jotai';

export const destinationsAtom = atom([]);
export const timesAtom = atom([]);
export const travelJourneyAtom = atom({
  origin : {
    destination: '',
    time: ''
  },
  to:[]
});
export const journeyAtom = atom([]);
export const emailAtom = atom('');
export const passwordAtom = atom('');
export const loadedAtom = atom(false);
export const minBudgetAtom = atom(0.00)