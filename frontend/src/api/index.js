import axios from "axios";
import toast from 'react-hot-toast';

const axiosInstance = axios.create({
  baseURL: "http://localhost:8080/api/",
  timeout: 5000,
  headers:{
    'content-type': 'application/json',
    "Access-Control-Allow-Origin": "*",
    "Accept": "application/json"
  }
})

const config = {headers:{'Content-Type': 'application/json',
"Access-Control-Allow-Origin": "*"}}
export const regex = {
  email: RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  ),
  password: RegExp(/^(?!\s*$).+/),
  budget:RegExp(/^[\d]+[\d]*$/),
  time: RegExp(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/)
}

export const login = async (email,password)=>{
 if(!regex.email.test(email) || !regex.password.test(password)){
    throw new Error("Please enter an email and password");
  }
 const response= await axiosInstance.post('/login',{email,password},config)
  return handleStatusCodes(response)
}
export const register = async(email,password)=>{
  if(!regex.email.test(email) || !regex.password.test(password)){
    throw new Error("Please enter an email and password");
  }
  const response= await axiosInstance.post('/register',{email,password},config)
  return handleStatusCodes(response)
}
export const allCities = async ()=>{
  const response= await axiosInstance.get('/all-cities',config)
  return handleStatusCodes(response)
}
export const allRoutes = async ()=>{
  const response= await axiosInstance.get('/all-routes',config)
  return handleStatusCodes(response)
}

export const plan = async (start,destination,time,budget)=>{
  
  const obj={start,destination,arrivalTime:time}

  if(budget){
    obj.budget = budget
  }
  const response= await axiosInstance.post('/plan',obj,config)
  return handleStatusCodes(response)
}

const handleStatusCodes = (response)=>{
  console.log("YES",response)
  const url = response.config.url;
  const success = ['login','register','plan'];
  switch(response.status){
    case 200:
    case 201: // Account Registration Success
      if(success.includes(url))toast.success(response.message,{id:'success'});
      break;
    default:
      toast.error(response.message??'Internal Server Error',{id:'load-error'});
      break;
  }
  return response;
}
export const getMinBudget = async()=>{
  const response= await axiosInstance.get('/min-budget',config)
  return handleStatusCodes(response)
}
export const loadData = async(setDestinations,setTimes,setMinBudget)=>{
  const asyncDispatch = [
    allCities(),
    allRoutes(),
    getMinBudget()
  ]
  const [dest,times,budget] = await Promise.all(asyncDispatch);
  // get unique times from an array of objects
  const filteredTimes = await getTimes(times.data)
  setDestinations(dest.data.sort());
  setTimes(filteredTimes.sort())
  setMinBudget(budget.data)
}
const getTimes = async(times)=>{
  const timeObj = {}
  //instead of doing index check for each time added, add to obj and just destructure keys
  for(let i = 0;i<times.length;i++){
    if(!(times[i].arrivalTime in timeObj)){
      timeObj[times[i].arrivalTime] = null
    }
  }
  return Object.keys(timeObj)
}