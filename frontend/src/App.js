import './App.css';
import { Routes, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import Header from './components/layout/Header';
import Home from './components/pages/Home';
import FAQ from './components/pages/FAQ';
import Four04 from './components/pages/Four04';
import TripPlanner from './components/pages/TripPlanner';
import Account from './components/pages/account/Account';
import toast, {Toaster} from 'react-hot-toast'
import { loadedAtom,destinationsAtom,timesAtom, minBudgetAtom } from './jotai';
import { useAtom } from 'jotai';
import { useEffect } from 'react';
import { loadData } from './api';

function App() {
  const [loaded,setLoaded] = useAtom(loadedAtom);
  const [,setDestinations]=useAtom(destinationsAtom);
  const [,setTimes]=useAtom(timesAtom);
  const [,setMinBudget]=useAtom(minBudgetAtom);
  useEffect(()=>{
    if(!loaded){
      loadData(setDestinations,setTimes,setMinBudget).then((res)=>{
        setLoaded(true);
      }).catch(err=>{console.log("ERROR:C",err);toast.error(err.msg??'Internal Server Error',{id:'load-error'})})
    }
  },[])
  return (
    <>
     <Toaster />
      <Helmet>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@1,500&display=swap" rel="stylesheet" />
      </Helmet>
      <Header/>
      <Routes>
        <Route path="*" element={<Four04/>} />
        <Route path="/" element={<Home/>} />
        <Route path="/faq" element={<FAQ/>} />
        <Route path="/trip-planner" element={<TripPlanner/>} />
        <Route path="/login" element={<Account/>} />
        <Route path="/register" element={<Account/>} />
        <Route path="/account" element={<Account/>} />
      </Routes>
    </>
  );
}

export default App;
